(function(){ "use strict";
	define([
		"jquery",
		"core/logger",
		"what-input"
	],
	function ($, Logger) {

		var start = function() {

			Logger.log('[PluginActivator] start');

		};

		// expose public fields
		return {
            start : start
        };
	});
})();
