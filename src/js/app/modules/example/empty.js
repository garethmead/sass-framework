/*
	Example / empty module template
*/

(function(){ "use strict";
	define(
	[
        "core/logger"
	],

	/* factory */
	function (Logger) {

		var 
			multiplyAB = function(a,b){
				return a * b;
			},
			
			init = function() {
				Logger.log('[Empty]::init - I\'m an empty / example module');

				const test = "this is a test";

				const hello = 'hello';
				const world = 'world';

				console.log(test);

				let myObject = {
					hello,
					world
				};

				console.log(myObject);

				const myArrowFunction = () => {
					console.log(myObject);
				};

				myArrowFunction();

			},

			start = function(){
				Logger.log('[Empty]::start - I\'m an empty / example module');
			};
			
		
		// expose public fields
		return {
            init : init,
            start : start,
            multiplyAB : multiplyAB
        };
	});
})();
