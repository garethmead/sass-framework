/*doc
---
title: Equal Heights
name: core/equalHeights
category: JS - core 
---
[`core/equalHeights`](../../assets/js/core/equalHeights.js)

Adds padding across rows of block-elements until they are all the same height.  
Uses [`core/breakpoints`](#core/breakpoints) to trigger a processing if/when the layout changes.


```html_example
<ul class="listing-item equal-heights super-list-2-items desk-list-2-items tab-list-1-items mob-list-1-items js-equal-heights-row">
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint minima, at laborum consequatur delectus quam esse, dolorum saepe molestiae. Fugit, quasi recusandae dicta in necessitatibus voluptate laudantium veritatis nisi deserunt quia non placeat facilis tempore perspiciatis laborum! Mollitia minus accusantium earum, similique iusto, dolorum reprehenderit provident esse fugiat. Enim, eligendi.
        </div>
    </li>
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            molestiae. Fugit, quasi recusandae dicta in necessitatibus voluptate laudantium veritatis nisi deserunt quia non placeat facilis tempore perspiciatis laborum! Mollitia minus accusantium earum, similique iusto, dolorum reprehenderit provident esse fugiat. Enim, eligendi.
        </div>
    </li>
</ul>
<br/>
<ul class="listing-item equal-heights super-list-6-items desk-list-3-items tab-list-3-items mob-list-1-items js-equal-heights-row">
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint minima, at laborum consequatur delectus quam esse, dolorum saepe molestiae. Fugit, quasi recusandae dicta in necessitatibus voluptate laudantium veritatis nisi deserunt quia non placeat facilis tempore perspiciatis laborum! Mollitia minus accusantium earum, similique iusto, dolorum reprehenderit provident esse fugiat. Enim, eligendi.
        </div>
    </li>
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci nihil sunt placeat necessitatibus facere explicabo rerum blanditiis nobis sequi voluptas?
        </div>
    </li>
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, corrupti!
        </div>
    </li>
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis consequuntur, quidem nam quod eligendi tenetur nisi necessitatibus, quas illum natus minus esse inventore ex vel est saepe. Consequatur expedita dolorem eos provident dolore eveniet, illum et tempora, accusantium. Sed quod aspernatur natus. Nesciunt numquam sint illum distinctio soluta quisquam fuga nobis, quis quo! Doloribus et dolores reprehenderit quia alias! Provident?
        </div>
    </li>
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, distinctio porro, perferendis atque eligendi culpa non esse! Repellendus eius voluptatem possimus est placeat porro, aliquid delectus unde odio, iure, deleniti laudantium qui a nobis error quos perferendis. Obcaecati, consectetur, laboriosam!
        </div>
    </li>
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, corrupti!
        </div>
    </li>
</ul>
<br/>
<ul class="listing-item equal-heights super-list-4-items desk-list-2-items tab-list-2-items mob-list-1-items js-equal-heights-row">
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Veritatis, corrupti! Adipisci nihil sunt placeat necessitatibus facere explicabo rerum blanditiis nobis sequi voluptas?
        </div>
    </li>
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </div>
    </li>
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Consequatur. Sed quod aspernatur natus. Nesciunt numquam sint illum distinctio
        </div>
    </li>
    <li>
        <div class="list-inner js-equal-heights-row-padding">
            Repellendus qui a nobis error quos perferendis. Obcaecati, consectetur, laboriosam!
        </div>
    </li>
</ul>
```
*/
 (function (window) { "use strict";

    define(["jquery", "underscore", "signals", "core/logger"],

        function ($, _, signals, Logger) {

            var
                EqualHeightsRow = ".js-equal-heights-row", // a row container for equal-heights elements
                EqualHeightsRowPadding = ".js-equal-heights-row-padding", // element that padding-bottom will be added to (any existing padding bottom will be respected)
                PaddingDataName = "eqHeightsPadding", // name for data obj to store existing padding-bottom values
				started = false,
                $rows = null, $rowPaddings = null,
                processed = new signals.Signal(),

                getMaxHeight = function ($group) {
                    var maxHeight = 0;
                    $group.each(function (i, o) {
						maxHeight = Math.max(maxHeight, $(o).height());
                    });
                    return maxHeight;
                },


                pixelsValueToInt = function (value) {
                    if (typeof value === "string") {
                        value = value.substring(0, value.indexOf("px"));
                    }
                    return parseInt(value, 10);
                },


                // set the padding bottom of an item
                setItemPadding = function ($item, maxHeight, paddingElementSelector) {
                    var itemHeight = $item.height();
                    if (itemHeight > 0 && itemHeight < maxHeight) {

                        var i       = $(paddingElementSelector, $item),
                            prePad  = pixelsValueToInt(i.data(PaddingDataName));

                        i.css("padding-bottom", prePad + (maxHeight - itemHeight));
                    }
                },


                // use a float as an object key - convert to string and replace . with _
                makeKey = function (value) {
                    return value.toString().replace(".", "_");
                },


                processRows = function($rows){

                    var $row = null, $rowChildren = null, maxRowHeight = null, $nested = null, offsetGroups = null;

                    $rows.each(function (i, o) {
                        $row = $(o);

                        if(!$row.data("processed")){ // not processed this row?

                            // look for nested rows within this row, and process them first.
                            $nested = $(EqualHeightsRow, $row);
                            if($nested.length > 0) {
                                processRows($nested); // recursive...
                            }

                            //Logger.log('processing row %o', $row);

                            // select all visible 'row' child elements
                            $rowChildren = $row.children().filter(":visible");
                            offsetGroups = {};

                            // Group items into rows by their vertical position in the page (based on offset.top)
                            // Needed to handle a row of elements wrapping onto more than one line in narrower layouts
                            $rowChildren.each(function () {
                                var offset = $(this).offset(),
                                    top = makeKey(offset.top);
                                if (_.isArray(offsetGroups[top]) === false) {
                                    offsetGroups[top] = [];
                                }
                                offsetGroups[top].push($(this));
                            });


                            // for each new pseudo row of elements
                            $.each(offsetGroups, function (ind, group) {
                                var $group = $(group);

                                // get the max row item height...
                                maxRowHeight = getMaxHeight($group);

                                // equalise the rest of the row elements with extra padding to match the max-height of the row
                                $group.each(function (ii, oo) {
                                    setItemPadding($(oo), maxRowHeight, EqualHeightsRowPadding);
                                });
                            });

                            $row.data('processed', true);
                        }

                        $row = $(o);
                    });
                },


                // equalise height of items by changing their padding-bottom
                equaliseRowItemHeights = function () {

					// reset
                    $rows.data('processed', false);
                    // remove any previously set padding-bottom
                    $rowPaddings.css("padding-bottom", 0);
                    // ...
                    processRows($rows);

                    processed.dispatch();
                },

                // debounce wrapper for equaliseRowItemHeights
                _equaliseRowItemHeights = _.debounce(equaliseRowItemHeights, 100),


				/**
				 * stop applying updates to paddings
				 * @param revertRowPaddings Bool - true and all extra padding will be removed
				 */
				stop = function(revertRowPaddings){
					if(started===true) {
						$(window).off('load resize orientationchange', _equaliseRowItemHeights);
						if(revertRowPaddings === true) {
							var $o;
							$rowPaddings.each(function (i, o) {
								$o = $(o);
								$o.css("padding-bottom", $o.data(PaddingDataName));
							});
						}
						started = false;
					}
				},

                /**
                 * Assign elements to a row using .equal-heights-row on containers,
				 * the immediate children of the container are the columns.
                 * Add a the .equal-heights-row-padding class either directly to a column,
				 * or any of its children - wherever you want the padding added.
                 */
                start = function(){

					stop(true);

                    Logger.log("[EqualHeights] start");

                    $rows = $(EqualHeightsRow);
                    $rowPaddings = $(EqualHeightsRowPadding, $rows);

                    if($rows.length > 0 && $rowPaddings.length > 0){

                        Logger.log("[EqualHeights] found data for %s rows:%o", $rows.length, $rows);

                        // store any existing padding-bottom
                        var $o;
						$rowPaddings.each(function (i, o) {
                            $o = $(o);
							$o.data(PaddingDataName, $o.css("padding-bottom"));
                        });

                        // update on load/resize/orientationchange (debounced)
                        $(window).on('load resize orientationchange', _equaliseRowItemHeights);
                        _equaliseRowItemHeights();

						started = true;

                    } else {
                        Logger.log("[EqualHeights] No valid equal-height rows/elements on this page.");
                    }
                };


            return {

                /**
                 * Start applying equalHeights, and watching for layout changes
                 */
                start: start,

                /**
                 * Stop watching for change, and optionally remove all padding added by equalHeights processing
                 * @param revertRowPaddings Bool, optional
                 */
				stop: stop,

                /**
                 * Signal - fired each time processing completes
                 */
                processed : processed
			};

});}(window));
