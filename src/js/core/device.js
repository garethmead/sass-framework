/*doc
---
title: Device
name: core/device
category: JS - core 
---
[`core/device`](../../assets/js/core/device.js)

Device info and utilities

*/
 (function (window, document) { "use strict";
    define(["jquery"],
        function ($) {
            var
                $win, userAgent,
				isiPod, isiPad, isiPhone, isAndroid, isBlackberry, isKindle, isIEMobile, isChrome, isFirefox, isSafari,
				hasTouch, hasMobileUserAgent, hasMobileQueryParam, isMobileOrTabletDevice, osName,
                
                getOSName = function() {
                    var appVer = navigator.appVersion, osName = "Unknown OS";
                    if (appVer.indexOf("Win") !== -1) { osName = "Windows"; }
                    else if (appVer.indexOf("Mac") !== -1) { osName = "MacOS"; }
                    else if (appVer.indexOf("X11") !== -1) { osName = "UNIX"; }
                    else if (appVer.indexOf("Linux") !== -1) { osName = "Linux"; }
                    return osName;
                },

                getIEVersion = function() {
                    var version = -1,
                        ua = navigator.userAgent,
                        re = (/MSIE ([0-9\.]+)/i),
                        result = re.exec(ua);

                    if (result !== null) {
                        try { version = parseFloat(result[1]); }
                        catch (err) { version = -1; }
                    } else {
                        if (navigator.appName === 'Netscape') { // of course. ie11 ua doesn't contain MSIE ....
                            // http://stackoverflow.com/questions/17907445/how-to-detect-ie11
                            re = new RegExp("Trident/.*rv:([0-9]{1,}[\\.0-9]{0,})");
                            if (re.exec(ua) !== null) {
                                version = parseFloat( RegExp.$1 );
                            }
                        }
                    }
                    return version;
                },

                isHighDPI = function() {
                    if (typeof window.devicePixelRatio !== 'undefined') {
                        if (window.devicePixelRatio > 1) { return true; }
                    }
                    return false;
                },

                setHtmlClass = function() {
                    if (isMobileOrTabletDevice === true) {
                        $("html").addClass("js-device-touch js-ready");
                    } else {
                        $("html").addClass("js-device-desktop js-ready");
                    }
                },

                init = function(){
                    $win = $(window);
                    userAgent = navigator.userAgent;
                    isiPod = userAgent.match(/iPod/i) !== null;
                    isiPad = userAgent.match(/iPad/i) !== null;
                    isiPhone = userAgent.match(/iPhone/i) !== null;
                    isAndroid = userAgent.match(/Android/i) !== null;
                    isChrome = userAgent.match(/Chrome/i) !== null;
                    isFirefox = userAgent.match(/Firefox/i) !== null;
                    isSafari = (userAgent.match(/Safari/i) !== null && userAgent.match(/Chrome/i) === null);
                    isBlackberry = userAgent.match(/BlackBerry/i) !== null;
                    isKindle = userAgent.match(/Kindle/i) !== null;
                    isIEMobile = userAgent.match(/IEMobile/i) !== null;
                    hasMobileUserAgent = /iPad|iPhone|iPod|android|kindle|IEMobile|BlackBerry|PlayBook|BB[\d]+|MeeGo/i.test(userAgent);
                    hasTouch = ('ontouchstart' in window) && ((navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));				
                    hasMobileQueryParam = window.location.href.indexOf("show-mobile-content=true") !== -1;
                    isMobileOrTabletDevice = hasMobileUserAgent || hasMobileQueryParam;
                    osName = getOSName();
                },

                start = function(){
                    setHtmlClass();
                };

            init();

        return {

			start: start,

			isLandscape: function() { var or = window.orientation; return (or === 90 || or === -90); },
			isPortrait: function() { return !this.isLandscape(); },

            getWindowWidth: function() { return $win.width(); },
            getWindowHeight: function() { return $win.height(); },
            getWindowAspectRatio: function() { return $win.width() / $win.height(); },

            screenAspectRatio: (screen.width / screen.height),
            screenWidth: screen.width,
            screenHeight: screen.height,

            hasTouch: hasTouch,
            hasMobileUserAgent: hasMobileUserAgent,
            isMobileOrTabletDevice: isMobileOrTabletDevice,
            hasHTML5VideoCapability: !!document.createElement('video').canPlayType,
            isHighDPI   : isHighDPI(),

            osName      : osName,
            isiOS       : (isiPod||isiPad||isiPhone),
            ieVersion   : getIEVersion(),
            isIE        : isIEMobile || getIEVersion() > -1,
			isiPod      : isiPod,
            isiPad      : isiPad,
            isiPhone    : isiPhone,
            isAndroid   : isAndroid,
            isChrome    : isChrome,
            isFirefox   : isFirefox,
            isSafari    : isSafari,
            isBlackberry: isBlackberry,
            isKindle    : isKindle,
            isIEMobile  : isIEMobile
        };
    });
}(window, document, navigator));