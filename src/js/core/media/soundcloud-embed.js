/*doc
---
title: Soundcloud Embed
name: core/media/soundcloud-embed
category: JS - core ~ media
---
[`core/media/soundcloud-embed`](../../assets/js/core/media/soundcloud-embed.js)

Utility that inserts a SoundCloud iframe player embed for a given id.

See https://developers.soundcloud.com/docs/api/html5-widget
See https://w.soundcloud.com/player/api_playground.html

In the example below, the container DIV  with the `data-audio-type="soundcloud"` atttribute will be replaced by a player iframe.
Attributes set on the div will be carried accross to the generated iframe. 

The `id` of the container is used to target each player. If you don't specify one in the mark-up one will be generated automatically.
 
Generated IDs will be in the format of `scloudiframe_[content-id]` OR `scloudiframe_[content-id]_[random-alphanumeric-string]` if there is more than one player with the same id...

```html_example
  <div>
        <br>
        <h4>A soundcloud player with the default configuration</h4>
        <div class="soundcloud-player">
            <div class="responsive-media-container">   
                <div class="responsive-media" data-audio-type="soundcloud-track" data-audio-id="188315965"></div>
            </div>
        </div>
    </div>
    
    <div>
        <br>
        <h4>A soundcloud player with some custom configuration options...</h4>
        <div class="soundcloud-player non-visual">
            <div class="responsive-media-container">   
                <div class="responsive-media"
                    data-audio-type="soundcloud-track" data-audio-id="192712540" 
                    data-sc-visual="false" 
                    data-sc-sharing="false" 
                    data-sc-liking="false" 
                    data-sc-download="false"
                    data-sc-color="f00"
                    ></div>
            </div>
        </div>
    </div>
```
*/

/* jshint unused:false */
(function(window, document){ "use strict";
	define(["jquery", "signals", "core/logger", "core/util/randomString",  "libs/soundcloud-api"],
	function ($, signals, Logger, randomString, API) {
        var 
            defaultPlayerOptions = {
                "auto_play"     :false,
                "buying"        :false,
                "liking"        :true,
                "download"      :true,
                "sharing"       :true,
                "show_artwork"  :true,
                "show_comments" :false,
                "show_playcount":false,
                "show_user"     :true,
                "hide_related"  :true,
                "color"         : undefined,
                "visual"        :true, // player type - the 'non-visual' layout is 164px high with artwork to the left.
                "start_track"   :0 // only applies to playlists - which are not implemented here yet
                //,callback        :function(){ Logger.log('Widget re-loaded.'); }
            },
            
            // TODO: support other player types -- playlists/favourites  
            iframeSrcBase = "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/",
            
            idPrefix = "scloudiframe_",
            
            players = {},
            playersReady = {},
            
            playerReady = new signals.Signal(),
            playerEvent = new signals.Signal(),
            
            
            // pull data-sc-[option_name] config options from the embed, or use the default values if not specified.
            getDataOptions = function($container){
                if(typeof $container === 'undefined') { return defaultPlayerOptions;  }
                var key, value, out = {};
                for(key in defaultPlayerOptions) {
                    value = $container.attr('data-sc-' + key);
                    out[key] = (typeof value === 'undefined') ? defaultPlayerOptions[key] : value;
                }
                return out;
            },
            
            
            getOptionsString = function($container){
                var out ='', key, opt = getDataOptions($container);
                for(key in opt){
                    if(key !== 'callback'){
                         out = out + ("&amp;" + key + "="  + opt[key]);
                    }
                }
                return out;
            },
            
            
            copyElementAttributes = function(elem){
                var out='', item, attr = elem.attributes,
                    i, n = attr.length;
                for(i=0;i<n;i++){
                    item=attr.item(i);
                    out += item.nodeName + '="' + item.nodeValue + '" ';
                }
                return out;
            },
            
            
            /**
             * Generate a unique dom id for a player. All players are accisible/identified by their id.
             * @param contentID
             * @returns {string}
             */
            generateContainerID = function(contentID){
                var id = idPrefix + contentID;   
                while(document.getElementById(id) !== null){ // if the id already exists... find a new one.
                    id = idPrefix + contentID + '_' + randomString();
                }
                return id;
            },
            
            
            getPlayerIsReady = function(id){
                return typeof playersReady[id] === 'undefined' ? false : playersReady[id];
            },
            
            
            getPlayer = function(id){
                return typeof players[id] === 'undefined' ? null : players[id];
            },
            
            
            onPlayerReady = function(id) {         
                Logger.log('[SoundcloudEmbed] onPlayerReady - %s', id);   
                playersReady[id] = true;
                
                var player = players[id], Events = API.Widget.Events,
                    playerEventHandler = function(type){ 
                        return function(data) {
                            onPlayerEvent(id, type, data);
                        };
                    };
                
                for(var key in Events) {
                    player.bind(Events[key], playerEventHandler(Events[key]));
                }
                
                playerReady.dispatch(player);
            },
            

            /**
             * Player Event handler - triggers the playerEvent signal, which emits the Player object, the event type, and any data about the event.
             * @param e
             */
            onPlayerEvent = function(playerId, type, data) {
                playerEvent.dispatch(players[playerId], type, data);   
            },

            
            /**
             * TODO: implement + test this
             * @param player - an existing player instance
             * @param newSourceURL - the new iframe/embed url
             * @param options
             */
            reconfigure = function(player, trackID, options){
                player.load(iframeSrcBase + trackID, options || defaultPlayerOptions);
            },
            
            
            /**
             * 
             * @param containerId
             * @param contentId - expects a soundcloud track id
             * @returns {boolean} true if a player was successfully created, false if not.
             */
            createPlayer = function(containerId, contentId){
                
                if(typeof players[containerId] !== 'undefined') {
                    Logger.warn('[SoundcloudEmbed] createPlayer - A player with the id "%s" has already been set up.', containerId);
                    return false;
                }
                
                Logger.log("[SoundcloudEmbed] createPlayer container-id:%s, track-id:%s", containerId, contentId);
                
                // 
                var $container = $('#' + containerId),
                    iframeSRC = iframeSrcBase + contentId + getOptionsString($container),
                    $iframe = $('<iframe src="' + iframeSRC + '" ' + copyElementAttributes($container.get(0)) + ' width="100%" height="100%" scrolling="no" frameborder="no"></iframe>'),
                    readyCallback = function() { onPlayerReady(containerId); },
                    player;                
                
                // replace the target container div with the embed iframe
                players[containerId] = player = null;
                playersReady[containerId] = false;
                //
                $container.replaceWith($iframe);
                
                try {                    
                    players[containerId] =  player = API.Widget($iframe.get(0));
                    // The Soundcloud player api doesn't appear to give access/reference back to it's containing element,
                    // so we're adding the iframe/container id to the player widget object as `ffid` here...
                    player.ffid = containerId;
                    player.bind(API.Widget.Events.READY, readyCallback);
                        
                } catch(err){
                    Logger.warn("[SoundcloudEmbed] Error setting up player API for %s", containerId);
                    Logger.warn(err);
                    return false;
                }
                
                return true;
            },
            
            
            /**
             * Set-up player(s) for a given selector
             * @param containerSelector - optional, defaults to 'div[data-audio-type="soundcloud-track"]'
             * 
             * @returns $containers - jQuery object of the DOM elements matching the container selector.
             */
            start = function(containerSelector){
                
                var selector = (typeof containerSelector === 'undefined') ? 'div[data-audio-type="soundcloud-track"]' : containerSelector,
                    $containers = $(selector),
                    n = $containers.length;
                
                if(n > 0){
                    
                    Logger.log('[SoundcloudEmbed] start - setting up a soundcloud iframe for %s element(s) matching the selector "%s"', n, selector);
                
                    var $container;
                    
                    $containers.each(function() {
                        $container = $(this);
                        if($container.is('iframe') === true) {
                            Logger.warn("[SoundcloudEmbed] start - the selected container is already an iframe... has it been set up already?");
                        }

                        var contentId = $container.attr('data-audio-id'),
                            containerId = $container.attr('id'); 
                        
                        // if the container already has an id, use that, otherwise generate one for it.
                        if (typeof containerId === 'undefined' || containerId.length === 0) {
                            containerId = generateContainerID(contentId);
                            $container.attr('id', containerId);
                        }
                        
                        if (typeof contentId === 'undefined' || contentId.length === 0) {
                            Logger.warn("[SoundcloudEmbed] start - embed container is missing the 'data-audio-id' attribute - unable to initialise.");
                            
                        } else {
                            Logger.log('[SoundcloudEmbed] start - setting up for element %s with conetntId %s', containerId, contentId);
                            createPlayer(containerId, contentId);
                        }
                    });
                }
                
                return $containers;
            };
        
		return { 
            //
			start               : start,
            // Signals
            playerReady         : playerReady, /* Signal<Player -> Void> */
            playerEvent         : playerEvent, /* Signal<Player -> EventType<String> -> Data<Dynamic> -> Void> */
            // 
            getPlayer           : getPlayer, /* by id */
            getPlayerIsReady    : getPlayerIsReady /* by id */
		};
	});
})(window, document);