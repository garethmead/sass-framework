(function () {
   'use strict';


    $('body').css('background-color', 'red');
    
    const test = "this is a test";
    
    const hello = 'hello';
    const world = 'world';
    
    console.log(test);
    
    let myObject = {
        hello,
        world
    };
    
    console.log(myObject);
    
    const myArrowFunction = () => {
        console.log(myObject);
    };
    
    myArrowFunction();

  
})();