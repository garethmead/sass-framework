/**
 * ...
 * @author Mike Almond - https://github.com/mikedotalmond
 *
 * Modernizr should be included in the head, before the requirejs app, 
 * this module checks for, and exports, the global window.Modernizr	
 */
(function(window) {
	define([], function () {
		if(typeof window.Modernizr === 'undefined'){
			throw "Oops! Modernizr needs to be included in the page <head> before requirejs and the application file(s).";
		}
		return window.Modernizr;
	});
})(window);