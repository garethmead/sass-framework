<?php include "./build-config/config.php"; ?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<title>Boilerplate html layout</title>

	<?php if (!empty($cssPath)) { ?>
		<link rel="stylesheet" href="<?php echo $cssPath; ?>">
	<?php } ?>

	<script src="<?php echo $assetPath; ?>js/libs/modernizr.js"></script>

</head>
<body>

	<div class="viewport-wide">

		<h1>BOILERPLATE DOCUMENT TO HELP SET UP BASE MARK UP FOR NEW TEMPLATES</h1>


		<header>

			Header content here

		</header>

		<main>

			Main page content here

		</main>

		<footer>


			Footer content here

		</footer>


	</div>

	<?php if(!empty($jsPath)) { ?>
		<script src="<?php echo $jsPath; ?>"></script>
	<?php } ?>

</body>
</html>
