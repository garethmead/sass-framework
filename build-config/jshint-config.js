module.exports = {
	"emitErrors": true,
	"failOnHint": true,
	"node": true,
	"browser": true,
	"esnext": true,
	"bitwise": true,
	"camelcase": true,
	"curly": true,
	"eqeqeq": true,
	"immed": true,
	"indent": 2,
	"latedef": false,
	"newcap": true,
	"noarg": true,
	"regexp": true,
	"undef": true,
	"unused": true,
	"strict": true,
	"trailing": false,
	"smarttabs": true,
	"globals" : {
		"$": true,
		"jQuery": true,
		"Modernizr": true,
		"Underscore": true,
		"define": true
	}
}
