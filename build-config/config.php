<?php 

	$assets = file_get_contents('./webpack-assets.json');
	$assetsJson = json_decode($assets, true);

	$prod = $assetsJson['metadata']['isProd'];
	$jsPath = $assetsJson['main']['js'];
	$cssPath = !empty($assetsJson['main']['css']) ? $assetsJson['main']['css'] : '';

	$assetPath = $prod === true ? '/dist/' : '//localhost:8080/dist/';

?>
