const babelPolyfill = require('babel-polyfill');
const webpack = require('webpack');

const path = require('path');
const glob = require('glob-all');

const ModernizrWebpackPlugin = require('modernizr-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin')
const CssoWebpackPlugin = require('csso-webpack-plugin').default;
const PurifyCSSPlugin = require('purifycss-webpack');

const assetPath = '../src/';
const jsPath = `${assetPath}js/`;
const sassPath = `${assetPath}sass/`;

const jsHintConfig = require('./jshint-config');

const isProd = process.env.NODE_ENV === 'production';

const cssDev = [
	'style-loader',
	{
		loader: 'css-loader',
		options: {
			sourceMap: true
		}
	},
	{
		loader: 'postcss-loader',
		options: {
			sourceMap: true,
			config: {
				path: './build-config/postcss.config.js'
			}
		}
	},
	{
		loader: 'sass-loader',
		options: {
			sourceMap: true,
			data: `$is-prod: ${isProd};`
		}
	}
];
const cssProd = ExtractTextPlugin.extract({
					fallback: ['style-loader'],
					use: [
						{loader: 'css-loader'},
						{
							loader: 'postcss-loader',
							options: {
								config: {
									path: './build-config/postcss.config.js'
								}
							}
						},
						{
							loader: 'sass-loader',
							options: {
								data: `$is-prod: ${isProd};`
							}
						}
					]
				});

const cssConfig = isProd ? cssProd : cssDev;

const projectInfo = require('./project-info.json');

const config = {
	context: path.resolve(__dirname, jsPath),
	entry: {
		main: ['babel-polyfill', './app/index.js']
	},
	devtool: isProd ? false : 'cheap-module-eval-source-map',
	output: {
		path: path.resolve(__dirname, '../dist'),
		publicPath: isProd ? '/dist/' : 'http://localhost:8080/dist/',
		filename: isProd ? '[name].bundle.[chunkhash].js' : '[name].bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',

						options: {
						  'presets': [
						    ['env', {
						      'targets': {
						        'browsers': ['last 2 versions']
						      }
						    }]
						  ]
						}

					}
				]
			},
			{
				test: /\.js$/, // include .js files
				enforce: 'pre', // preload the jshint loader
				exclude: [
					path.resolve(__dirname, `${jsPath}libs`),
					/node_modules/
				], // exclude any and all files in the node_modules folder
				use: [
					{
						loader: 'jshint-loader',
						options: jsHintConfig
					}
				]
			},
			{
				test: /\.scss$/,
				use: cssConfig
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: 'images/[name].[ext]'
						}
					},
					{
						loader: 'image-webpack-loader',
						query: {
							svgo: {
								plugins: [
									{
										removeTitle: true
									}
								]
							}
						}
					}
				]
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/,
				use: [
					'file-loader'
				]
			}
		],
	},

	devServer: {
		compress: true,
		hot: true,
		overlay: true,
		stats: 'errors-only',
		headers: {
			'Access-Control-Allow-Origin': '*'
		},
		host: 'localhost'
	},
	resolve: {
		modules: [path.resolve(__dirname, jsPath), 'node_modules']
	},
	plugins: [
		new webpack.optimize.ModuleConcatenationPlugin(),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			_: 'underscore'
		}),
		new ModernizrWebpackPlugin({
				'filename': 'js/libs/modernizr.js',
				'feature-detects': [],
				'extra': {
					'shiv': true,
					'printshiv': false,
					'load': true,
					'mq': false,
					'cssclasses': true
				},
				'options': [
					'setClasses'
				]
		}),
		new ExtractTextPlugin({
			filename: 'css/style.[contenthash].css',
			disable: !isProd,
			allChunks: true
		}),
		new webpack.NamedModulesPlugin(),
		new webpack.DefinePlugin({
			FRAMEWORK_NAME: JSON.stringify(projectInfo.name),
			FRAMEWORK_VERSION: JSON.stringify(projectInfo.version),
			PROJECT_NAME: JSON.stringify(projectInfo.name),
			PROJECT_VERSION: JSON.stringify(projectInfo.version),
			PROJECT_DESCRIPTION: JSON.stringify(projectInfo.description),
			PROJECT_BUILD_TIME: JSON.stringify(new Date().toString()),
			DEBUG: JSON.stringify(!isProd)
		}),
		new StyleLintPlugin({
			configFile: './build-config/.stylelintrc.js',
			context: path.resolve(__dirname, sassPath),
			syntax: 'scss'
		}),
		new AssetsPlugin({metadata: {isProd: isProd}}),
		new CssoWebpackPlugin(),
		new PurifyCSSPlugin({
			// Give paths to parse for rules. These should be absolute!
			paths: glob.sync([
				path.join(__dirname, '../*.php'),
				path.join(__dirname, '../_page-partials/*.php'),
				path.join(__dirname, '../_page-partials/hero/*.php'),
				path.join(__dirname, '../_page-partials/navigation/*.php')
				//path.join(__dirname, '../anchor-careers-components.html')
			]),
			purifyOptions: {
				whitelist: ['*slick*', '*select2*', '*ps*', '*magic-line*', '*active*', '*ignore-default*', '*wistia*']
			}
		})
	]
};

if(!isProd) {
	config.plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = config;
