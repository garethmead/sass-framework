"use strict";

module.exports = {
  "extends": "stylelint-config-standard",
  "ignoreFiles": "../src/sass/**/vendors/**/*.scss",
  "rules": {
    "at-rule-empty-line-before": null,
    "at-rule-no-unknown": [ true, {
      "ignoreAtRules": [
        "mixin",
        "function",
        "at-root",
        "if",
        "else",
        "include",
        "warn",
        "return",
        "for",
        "each",
        "extend",
        "content",
        "error",
        "while"
      ]
    }],
    "block-closing-brace-empty-line-before": null,
    "block-closing-brace-newline-after": ["always", {
      ignoreAtRules: ["if", "else"]
    }],
    "color-named": "never",
    "comment-whitespace-inside": null,
    "declaration-empty-line-before": null,
    "indentation": "tab",
    "max-empty-lines": 2,
    "no-duplicate-selectors": true,
    "no-eol-whitespace": null,
    "rule-empty-line-before": null,
    "selector-list-comma-newline-after": null,
    "string-quotes": "double"
  }
}
