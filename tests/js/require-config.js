/*
* NOTE: this contains data manually copied from the main (assets/js/app/require-config) project config. 
*
* You need to manually update this file if any framework libraries in app/require-config are added or changed.
*
* The baseUrl is the same as for the main app, so it should just be a case
* of copying the appropriate paths or shim config as needed
*
*/

var globaljQuery = false;
var GreenSockGlobals = {}; // required by GSAP

var require = {
	map: { 
		/* https://stackoverflow.com/questions/16736663/require-js-jquery-noconflict-shim-why-is-init-method-never-called */
		// '*' means all modules will get 'jquery-private' for their 'jquery' dependency.
		'*': { 'jquery': 'jquery-private' },
		// 'jquery-private' wants the real jQuery module though.
		// If this line was not here, there would be an unresolvable cyclic dependency.
		'jquery-private': { 'jquery': 'jquery' }
	},
	
	paths: {
		jquery: "libs/jquery/jquery-1.11.1.min",
		"jquery-private": "libs/jquery/jquery-private",
		
		// jQuery related
		parsley: "libs/jquery/utils/parsley-2.0.0",
		slick: "libs/jquery/utils/slick",
		placeholder:'libs/jquery/utils/jquery.placeholder',
		fancybox:'libs/jquery/utils/jquery.fancybox',
		
		underscore:"libs/underscore",
		
		/* Modernizr should be included in the head, before the requirejs app. This module checks for that, and exports the global window.Modernizr */
		Modernizr:"libs/modernizr-module",

		/* https://github.com/millermedeiros/js-signals */
		signals: "libs/signals",
		
		// NOTE: Use 'core/tween' module to get TweenLite with the CSS jquery plugins enabled.
		TweenLite:"libs/greensock/TweenLite"
	},

	shim: {
		TweenLite: { exports:"GreenSockGlobals.TweenLite" },
		"libs/soundcloud-api": { exports:"window.SC" }	
	}	
};
