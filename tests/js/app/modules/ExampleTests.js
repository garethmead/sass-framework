define(function (require) {

	var 
		// import what we're testing...
		example = require('app/modules/example/empty');
		
	
	// Define the QUnit module and life-cycle.
	QUnit.module("app/modules/Example", {
		setup: function () {
			
		},
		teardown: function () {
			
		}
	});

	
	//
	// Tests
	// @see http://api.qunitjs.com/category/assert/
	//
	
	// Some basic property checks... ensures you implement init and start functions for modules
	QUnit.test("Example - module exists and init + start are functions", function (assert) {
		assert.notEqual(typeof example, 'undefined');
		assert.strictEqual(typeof example.init, 'function');
		assert.strictEqual(typeof example.start, 'function');
	});
	
	// 
	QUnit.test("Example - multiplyAB", function (assert) {
		var a = (Math.random()*100)|0;
		var b = (Math.random()*100)|0;
		var result = example.multiplyAB(a, b);
		// example.multiplyAB multiplies two numbers together and returns the result.
		assert.strictEqual(a*b, result,  a + " * " + b + " =  " + result); 
	});
});
