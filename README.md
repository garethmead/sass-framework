# fframework

## What?
Continuous compilation and validation of your front-end code; just `npm` it.

- `npm run dev` - runs the dev task with hot module replacement enabled
- `npm run prod` - compiles and compresses files into the `dist` package

Once started, the dev build will run whenever files in the project change.

Have a look at the [Webpack config](webpack.config.js) that configures all of this if you're feeling fruity!


## Lint
Javascript is linted using [JSHint](http://jshint.com/install/). See `.jshintrc` for specifics on what is linted. Javascript libraries such as jQuery plugins are ignored.
Please ensure these are added to the `libs` folder or installed using `npm`. Javascript will fail to compile unless all files are error free.

Sass is linted using [Stylelint](https://github.com/stylelint/stylelint). See `.stylelintrc.js` for specifics on what is linted. Libraries are ignored.
Sass will fail to compile unless all files are error free

## Modern
fframework supports ES6 and transpiles code using [BabelJS](https://babeljs.io/)

The latest CSS features are also supported and transpiled using [PostCSS](http://postcss.org/) and the [cssnext plugin](http://cssnext.io/)

## Test
~~JavaScript unit tests are completed using [QUnit](https://qunitjs.com/) and phantomJS.~~

We're still trying to find the best solution for unit tests. fframework was previously powered using Grunt but this is no longer the case as we try to streamline our build process

---

## System Requirements
- [NodeJS / NPM](http://nodejs.org/)

NPM and Webpack will take care of the rest.

## Getting Started

`$ npm install`
Install all of the packages required by Webpack.

---

## Whom?
* [Gareth](https://twitter.com/garethmead) - concept, html, sass, build
* [Mike](https://twitter.com/mikedotalmond) - js, build
